bin/cifrado: obj/main.o obj/ciclico.o obj/mod.o obj/autollave.o
	gcc -Wall -I./include $^ -o $@
obj/main.o: src/main.c include/cifrado.h
	gcc -Wall -c -I./include $< -o $@
obj/ciclico.o: src/ciclico.c include/cifrado.h
	gcc -Wall -c -I./include $< -o $@
obj/autollave.o: src/autollave.c include/cifrado.h
	gcc -Wall -c -I./include $< -o $@
obj/mod.o: src/mod.c include/cifrado.h
	gcc -Wall -c -I./include $< -o $@
clean:
	rm -f obj/* bin/*