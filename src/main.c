#include "cifrado.h"

int main(int argc, char **argv){
	if(argc!=5){
		fprintf(stderr, "%s\n", "Not enough arguments\nUsage: ./cifrado -t <ciclico|autollave> -p <key>");
		return -1;
	}
	char message[MAXLINE]={0};
	fgets(message, MAXLINE, stdin);
	if(strcmp(argv[2],"ciclico")==0){
		ciclico(message,atoi(argv[4]));
	}else if(strcmp(argv[2],"autollave")==0){
		autollave(message,argv[4]);
	}else{
		fprintf(stderr, "%s\n", "Argument not recognized");
		return -1;
	}
	printf("%s", message);
	
	return 0;
}