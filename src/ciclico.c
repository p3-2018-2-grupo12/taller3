#include "cifrado.h"

void ciclico(char *s, int n){
	while(*s){
		if(isalpha(*s)){
			if(isupper(*s)){
				*s=(mod((*s-'A'+n),(26)))+('A');
			}else{
				*s=mod((*s-'a'+n),(26))+('a');
			}
		}
		s++;
	}
}