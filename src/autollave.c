#include "cifrado.h"
char getChar(int *i, char *text, int len){
	while(isspace(text[*i%len]) || !isalpha(text[*i%len])){
		(*i)++;
	}
	return text[(*i)++%len];
}
void autollave(char *text, char *key){
	int len = strlen(text);
	int left=0;
	char tmp[len+1];
	tmp[len]='\0';
	for(int i = 0;i<len;i++){
		tmp[i] = text[i];
	}

	for(int i = 0;i<len;i++){
		while(*key && (isspace(*key) || !isalpha(*key))){
			key++;
		}
		if(isalpha(text[i])){
			if(isupper(text[i])){
				if(*key){
					text[i] = (text[i]+toupper(*key)-'A'-'A')%26+'A';
					key++;
				}else{
					text[i] = (text[i]+toupper(getChar(&left,tmp,len))-'A'-'A')%26+'A';
				}
				
			}else{
				if(*key){
					text[i] = (text[i]+tolower(*key)-'a'-'a')%26+'a';
					key++;
				}else{
					text[i] = (text[i]+tolower(getChar(&left,tmp,len))-'a'-'a')%26+'a';
				}
			}
		}
	}
}

